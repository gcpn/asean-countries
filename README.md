# Members of ASEAN

Program sederhana untuk menampilkan nama negara beserta nama ibukota dan gambar bendera (dari asset bawaan app).

Widget utama yang digunakan:

```dart
class AseanScreen extends StatelessWidget {
  AseanScreen({Key key, this.title, @required this.aseanCountries})
      : super(key: key);

  final String title;
  // list yang berisi negara-negara,
  final List<AseanCountry> aseanCountries;

  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(this.title),
        ),
        body: ListView.builder(
            itemCount: this.aseanCountries.length,
            itemBuilder: (context, index) {
              return ListTile(
                leading: CircleAvatar(
                  backgroundImage: AssetImage(this.aseanCountries[index].flag),
                  radius: 45,
                ),
                title: Text(this.aseanCountries[index].name),
                subtitle: Text(this.aseanCountries[index].capital),
              );
            }));
  }
}
```

#### by Gottfried / DTS TA FE

### Screenshots

![1](https://gitlab.com/gcpn/asean-countries/-/raw/master/screenshots/2.png)

![2](https://gitlab.com/gcpn/asean-countries/-/raw/master/screenshots/1.png)