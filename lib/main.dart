import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

void main() {
  runApp(MyApp());
}

/// Kelas representasi negara dengan nama negara [name],
/// nama ibukota [capital], dan path bendera [flag]
class AseanCountry {
  final String name;
  final String capital;
  final String flag;

  AseanCountry(this.name, this.capital, this.flag);
}

class MyApp extends StatelessWidget {
  // Instansiasi negara-negara Asean di dalam fungsi ini,
  // kembalikan list
  List<AseanCountry> generateAseanCountries() {
    AseanCountry indonesia =
        new AseanCountry('Indonesia', 'Jakarta', 'assets/images/indonesia.png');
    AseanCountry malaysia = new AseanCountry(
        'Malaysia', 'Kuala Lumpur', 'assets/images/malaysia.png');
    AseanCountry brunei = new AseanCountry(
        'Brunei', 'Bandar Seri Begawan', 'assets/images/brunei.png');
    AseanCountry singapore = new AseanCountry(
        'Singapore', 'Singapore', 'assets/images/singapore.png');
    AseanCountry thailand =
        new AseanCountry('Thailand', 'Bangkok', 'assets/images/thailand.png');
    AseanCountry vietnam =
        new AseanCountry('Vietnam', 'Hanoi', 'assets/images/vietnam.png');
    AseanCountry laos =
        new AseanCountry('Laos', 'Vientiane', 'assets/images/laos.png');
    AseanCountry myanmar =
        new AseanCountry('Myanmar', 'Naypyidaw', 'assets/images/myanmar.png');
    AseanCountry philippines = new AseanCountry(
        'Philippines', 'Manila', 'assets/images/philippines.png');
    AseanCountry cambodia = new AseanCountry(
        'Cambodia', 'Phnom Penh', 'assets/images/cambodia.png');

    return [
      indonesia,
      malaysia,
      singapore,
      brunei,
      thailand,
      vietnam,
      laos,
      myanmar,
      philippines,
      cambodia
    ];
  }

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
        // This makes the visual density adapt to the platform that you run
        // the app on. For desktop platforms, the controls will be smaller and
        // closer together (more dense) than on mobile platforms.
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: AseanScreen(
        title: 'Members of ASEAN',
        aseanCountries: generateAseanCountries(),
      ),
    );
  }
}

class AseanScreen extends StatelessWidget {
  AseanScreen({Key key, this.title, @required this.aseanCountries})
      : super(key: key);

  final String title;
  // list yang berisi negara-negara,
  // diberikan via konstruktor
  final List<AseanCountry> aseanCountries;

  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(this.title),
        ),
        body: ListView.builder(
            itemCount: this.aseanCountries.length,
            itemBuilder: (context, index) {
              return ListTile(
                leading: CircleAvatar(
                  backgroundImage: AssetImage(this.aseanCountries[index].flag),
                  radius: 45,
                ),
                title: Text(this.aseanCountries[index].name),
                subtitle: Text(this.aseanCountries[index].capital),
              );
            }));
  }
}
